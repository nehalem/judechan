using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace judechan.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /HelloWorld/

        public string Index()
        {
            return "This is my default action...";
        }

        //
        // GET: /HelloWorld/Welcome/

        public IActionResult Welcome(string name, int numtimes = 1)
        {
            ViewData["Message"] = $"Hello {name}";
            ViewData["NumTimes"] = numtimes;
            return View();
        }
    }
}
